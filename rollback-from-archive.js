const fs = require('fs');
const json = fs.readFileSync('environment.json');
const ENV_contents = JSON.parse(json);
var arch_folder = 'cicd-archive-stan'
var data;


var AWS = require("aws-sdk");
AWS.config.update({ region: ENV_contents.BLUE_REGION });
var s3 = new AWS.S3();

console.log( "###rollback-from-archive\n");
console.log( "###PREREQUISITE: MAKE SURE TAR FILE TO ROLLBACK HAS BEEN RESTORED VIA AWS CONSOLE\n");
console.log( "###This script accomplishes the following:");
console.log( "###1. User to provide the filename of the tar file that needs to be redeployed");
console.log( "###2. Check which bucket is green, non-active");
console.log( "###3. Move extracted files to bucket mentioned in #2\n");


// Get process.stdin as the standard input object.
var standard_input = process.stdin;
var tarFile;
var filename;
var foldername;
var environment;
var appname;
var syncdirectory;
var key;

// Set input character encoding.
standard_input.setEncoding('utf-8');

var myArgs = process.argv.slice(2);

if( myArgs[0] == null || myArgs[0] == undefined ){
	
	console.log("tar file directory is needed!");
	process.exit(1);
	
} else {
	tarFile = myArgs[0];
}

       
if( tarFile.includes('sp7') || tarFile.includes('swp') ){
	
	console.log("FILE TO ROLLBACK IS: " + tarFile);
	
	filename = tarFile.substring(tarFile.lastIndexOf("/") + 1);
	syncdirectory = tarFile.substring(0, tarFile.lastIndexOf("/"));
	foldername = syncdirectory.substring(syncdirectory.lastIndexOf("/") + 1);
	environment = foldername.split("-").slice(-2,-1); //Get the string from the 2nd to the last occurence of -

	console.log("\nFILENAME - " + filename);
	console.log("SYNCDIRECTORY - " + syncdirectory);
	console.log("FOLDERNAME - " + foldername);
	console.log("ENVIRONMENT - " + environment);

	var key = tarFile.replace("s3://" + arch_folder + "/","");
	console.log("KEY - " + key);
	appname = key.substring(0,key.indexOf("/"));
	console.log("APPNAME - " + appname);
	
	
	var cicdTable = ENV_contents.CICD_TABLE
	
	const queryBucket = {
				ExpressionAttributeValues: {
					":color1": { S: "green" },
					":active1": { BOOL: false }
				},
				ProjectionExpression: "s3bucketname",
				FilterExpression: "color = :color1 AND active = :active1",
				TableName: cicdTable
	};
	
	var dynamodb = new AWS.DynamoDB();
	
	dynamodb.scan(queryBucket, function(err, data) {
	   if (err) console.log(err, err.stack); // an error occurred
	   else {
			
			if(data.Count == 0){
				console.log("No blue, non-stable, zero-weighted bucket found! Rollback cancelled.");
				process.exit(1);
			} else if(data.Count == 1){
				console.log("green, non-active bucket found. Rollback to commence.");
				console.log(data.Items[0].s3bucketname.S);
				
				var bucketname = data.Items[0].s3bucketname.S;
				
				const bucketParams = {
				   Bucket : bucketname
				};
				
				s3.headBucket(bucketParams, function(err) {
					
					if (err) {
						console.log("Bucket name specified on dynamo DB not existing in S3!. Exiting...");
						process.exit(1);
					} else {
						console.log("Bucket in S3 found. Processing Tar file...");
						processTar(bucketname);
					}
					
				});
				
				
			} else {
				console.log("please investigate cicd table for possible data issues");
				process.exit(1);
			}
	   }
	   
	 });
	 
	
} else {
	
	console.log("FILENAME INVALID, SHOULD BE swp OR sp7");
	
}


function processTar(destination){
	var dest = destination;
	
	var exec = require('child_process').execSync;
	
	console.log("Creating temporary location");
	
	if (fs.existsSync(foldername)) {
	console.log("Deleting extracted files from temp location...");
			var delete_tmp_folder = exec(`rm -rf ${foldername}`, function(error, stdout, stderr) {
			  if (error) { 
				  console.log(error);
				  process.stdout.write(stdout);
				  process.stderr.write(stderr);
				  process.exit(1);
			  }
			});
	} 
	
	var sync_mkdir = exec(`mkdir ${foldername}`, function(error, stdout, stderr) {
		if (error) {
				console.log(error);
				process.exit(1);
				};
        });
	
	console.log("Downloading tar file to temp location...");
	var sync_download = exec(`aws s3 sync ${syncdirectory} ${foldername}`, function(error, stdout, stderr) {
	  if (error){ 
		console.log(error);
		process.exit(1);
		}
	});
	console.log("Extracting tar file...");
	var sync_untar = exec(`tar -C ${foldername} -xvf ${foldername}/${filename}`, function(error, stdout, stderr) {
	  if (error){ 
		console.log(error);
		process.exit(1);
		}
	});
	console.log("Uploading extracted files to the ff S3 bucket: "+dest);
	var sync_upload = exec(`aws s3 sync ${foldername}/${foldername} s3://${dest} --delete`, function(error, stdout, stderr) {
	  if (error){ 
		console.log(error);
		process.exit(1);
		}
	});
   
   
   if (fs.existsSync(foldername)) {
	console.log("Deleting extracted files from temp location...");
			var delete_tmp_folder = exec(`rm -rf ${foldername}`, function(error, stdout, stderr) {
			  if (error){
				  console.log(error);
				process.stdout.write(stdout);
				process.stderr.write(stderr);
			  }
			});
	} 
	
}