#!/bin/sh
key=$1;
CICD_DIR="cicd-archive"

echo "input tar is: ${key}"

s3bucket="s3://$CICD_DIR/"
key=`sed "s|$s3bucket||g"<<<$key`

parms="{\"Days\":1,\"GlacierJobParameters\":{\"Tier\":\"Standard\"}}"
#echo $parms;

type=`aws s3api head-object --bucket $CICD_DIR --key $key --query Restore --output text`
if [ $? -eq 0 ]; then

	#If there is no restore request, it means it is still in glacier, then we need to call restore-object command
	#ongoing-request key will not be returned if tar file is glacier
	if [[ $type != *'ongoing-request'* ]]; then
		echo "Restoring file..."
		aws s3api restore-object --bucket $CICD_DIR --key $key --restore-request $parms
		if [ $? -ne 0 ]; then
			echo "Problem on restore-object request. Exiting"
			exit 1;
		fi
	#if ongoing-request=false, it means glacier restore was successful already, no need to poll
	elif [[ $type == *'ongoing-request="false"'* ]]; then
		echo "File already restored, no need to continually poll for status."
		exit 0;
	fi
	
	#poll for status change
	while :
	do
		sleep 5m;
		type=`aws s3api head-object --bucket $CICD_DIR --key $key --query Restore --output text`
		if [[ $type == *'ongoing-request="false"'* ]]; then
			echo "File has finally been restored"
			break;
		else
			echo "File still restoring"
		fi	
		done
else
    echo "error on head-object command. Exiting"
	exit 1;
fi


