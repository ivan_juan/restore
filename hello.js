//Get process.stdin as the standard input object.
var standard_input = process.stdin;

// Set input character encoding.
standard_input.setEncoding('utf-8');

var myArgs = process.argv.slice(2);
var data;

if( myArgs[0] == null || myArgs[0] == undefined ){
	
	console.log("tar file directory is needed!");
	process.exit(1);
	
} else {
	data = myArgs[0];
}

console.log("Hello "+data);